import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

const styles = {
    basic: {
        borderLeft: '1px solid transparent',
        borderRight: '1px solid transparent',
    },
    green: {
        border: '1px solid #eee',
        backgroundColor: 'green',
    },
    ligthYellow: {
        border: '1px solid #eee',
        backgroundColor: 'yellow',
    },
    orange: {
        border: '1px solid #eee',
        backgroundColor: 'orange',
    },
    red: {
        border: '1px solid #eee',
        backgroundColor: '#c91d12',
    },
    darkRed: {
        border: '1px solid #eee',
        backgroundColor: 'rgba(139, 0, 0, 1)',
    },
};

const getRange = (value) => {
    if (value < .35) {
        return 'green';
    } else if (value >= .35 && value < .5) {
        return 'ligthYellow';
    } else if (value >= .5 && value < .65) {
        return 'orange';
    } else if (value >= .65 && value < .8) {
        return 'red';
    } else if (value >= .8) {
        return 'darkRed';
    }
};

const TableRowItem = ({ classes, showColours, review: n }) => (
    <TableRow hover tabIndex={-1} key={n.id}>
        <TableCell padding="checkbox">
            {n.id}
        </TableCell>
        <TableCell component="th" scope="row" style={{ padding: '16px 8px' }}>
            {n.body}
        </TableCell>
        <TableCell numeric padding="dense"
            className={classNames(classes.basic, showColours && classes[getRange(n.toxic)])}
        >{n.toxic}</TableCell>
        {/* <TableCell numeric padding="dense"
            className={classNames(classes.basic, showColours && classes[getRange(n.severe_toxic)])}
        >{n.severe_toxic}</TableCell> */}
        <TableCell numeric padding="dense"
            className={classNames(classes.basic, showColours && classes[getRange(n.obscene)])}
        >{n.obscene}</TableCell>
        <TableCell numeric padding="dense"
            className={classNames(classes.basic, showColours && classes[getRange(n.threat)])}
        >{n.threat}</TableCell>
        <TableCell numeric padding="dense"
            className={classNames(classes.basic, showColours && classes[getRange(n.insult)])}
        >{n.insult}</TableCell>
        <TableCell numeric padding="dense"
            className={classNames(classes.basic, showColours && classes[getRange(n.identity_hate)])}
        >{n.identity_hate}</TableCell>
    </TableRow>
);

TableRowItem.propTypes = {
    classes: PropTypes.object.isRequired,
    review: PropTypes.object.isRequired,
    showColours: PropTypes.bool.isRequired,
};

export default withStyles(styles)(TableRowItem);
