import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';

import EnhancedTableToolbar from './EnhancedTableToolbar';
import EnhancedTableHead from './EnhancedTableHead';
import EE_predictions from './EE_predictions';
import TableRowItem from './TableRowItem';


let counter = 0;
const createData = () => {
    return EE_predictions.map(prediction => {
        counter += 1;
        return {
            id: counter,
            ...prediction
        };
    });
};

const getMean = (review) => {
    return (
        review['toxic']
        + review['severe_toxic']
        + review['obscene']
        + review['threat']
        + review['insult']
        + review['identity_hate']
    ) / 6;
};

const getSorting = (order, orderBy) => {
    if (orderBy === 'average') {
        return order === 'desc'
            ? (a, b) => getMean(b) - getMean(a)
            : (a, b) => getMean(a) - getMean(b);
    }
    return order === 'desc'
        ? (a, b) => b[orderBy] - a[orderBy]
        : (a, b) => a[orderBy] - b[orderBy];
};



const styles = theme => ({
    root: {
        width: '100%',
    },
    table: {
        minWidth: 1020,
    },
    tableWrapper: {
        overflowX: 'auto',
    },
});

class EnhancedTable extends React.Component {
    state = {
        order: 'asc',
        orderBy: 'calories',
        data: createData(),
        page: 0,
        rowsPerPage: 50,
        showColours: true,
    };

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';
        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }
        this.setState({ order, orderBy });
    };

    handleChangePage = (event, page) => this.setState({ page });

    handleChange = (key, value) => { this.setState({ [key]: value }) }

    handleChangeRowsPerPage = event => {
        this.setState({ rowsPerPage: event.target.value });
    };

    handleFilterBySeachTerm = (reviewBody, searchTerm) => {
        const text = reviewBody.toString().toLowerCase();
        return text.indexOf(searchTerm.toLowerCase()) > -1;
    };

    render() {
        const { classes } = this.props;
        const { data, order, orderBy, rowsPerPage, page, searchTerm, showColours } = this.state;

        return (
            <Paper className={classes.root}>
                <EnhancedTableToolbar
                    showColours={showColours}
                    onClickSortByAverage={this.handleRequestSort}
                    onChangeSearchbox={searchTerm => this.handleChange('searchTerm', searchTerm)}
                    onShowColors={checked => this.handleChange('showColours', checked)}
                />
                <div className={classes.tableWrapper}>
                    <Table className={classes.table} aria-labelledby="tableTitle">
                        <EnhancedTableHead
                            order={order}
                            orderBy={orderBy}
                            onRequestSort={this.handleRequestSort}
                        />
                        <TableBody>
                            {data
                                .filter(review => {
                                    if (searchTerm && searchTerm !== '') {
                                        return this.handleFilterBySeachTerm(review.body, searchTerm);
                                    } else {
                                        return true;
                                    }
                                })
                                .sort(getSorting(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map(n => <TableRowItem key={n.id} review={n} showColours={showColours} />)}
                        </TableBody>
                    </Table>
                </div>
                <TablePagination
                    component="div"
                    count={data.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    rowsPerPageOptions={[30, 50, 100]}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                />
            </Paper>
        );
    }
}

EnhancedTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EnhancedTable);