import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

const styles = theme => ({
    root: {
        padding: `${theme.spacing.unit}px ${theme.spacing.unit * 3}px`,
    },
    spacer: {
        flex: '1 1',
    },
    actions: {
        color: theme.palette.text.secondary,
    },
    title: {
        flex: '0 0 auto',
    },
});

class EnhancedTableToolbar extends Component {
    handleChangeCheckbox = event => this.props.onShowColors(event.target.checked);

    render() {
        const { classes, showColours, onClickSortByAverage, onChangeSearchbox } = this.props;
        return (
            <Toolbar className={classes.root}>
                <div className={classes.title}>
                    <FormGroup row>
                        <Typography variant="title" id="tableTitle">My EE</Typography>
                        <Typography variant="caption" id="tableTitle">Google PlayStore reviews</Typography>
                    </FormGroup>
                </div>
                <div className={classes.spacer} />
                <div className={classes.actions}>
                    <FormGroup row>
                        <FormControlLabel
                            style={{ marginTop: 10 }}
                            control={
                                <Checkbox
                                    checked={showColours}
                                    onChange={this.handleChangeCheckbox}
                                />
                            }
                            label="Colour code"
                        />
                        <TextField
                            onChange={event => onChangeSearchbox(event.target.value)}
                            label="Search"
                            name="search"
                            style={{ margin: '0 8px' }}
                        />
                        <Button variant="outlined" onClick={event => onClickSortByAverage(event, 'average')}>Sort by Average</Button>
                    </FormGroup>
                </div>
            </Toolbar>
        );
    }
}

EnhancedTableToolbar.propTypes = {
    classes: PropTypes.object.isRequired,
    onClickSortByAverage: PropTypes.func.isRequired,
    onChangeSearchbox: PropTypes.func.isRequired,
    onShowColors: PropTypes.func.isRequired,
    showColours: PropTypes.bool.isRequired,
};

export default withStyles(styles)(EnhancedTableToolbar);