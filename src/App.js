import React, { Component } from 'react';
import EnhancedTable from './EnhancedTable';

class App extends Component {
  render() {
    return <EnhancedTable />;
  }
}

export default App;
