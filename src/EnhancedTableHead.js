import React from 'react';
import PropTypes from 'prop-types';
import Tooltip from '@material-ui/core/Tooltip';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

const rows = [
    { id: 'id', numeric: true, disablePadding: true, label: 'ID' },
    { id: 'body', numeric: false, disablePadding: false, label: 'Content' },
    { id: 'toxic', numeric: true, disablePadding: true, label: 'Toxic' },
    // { id: 'severe_toxic', numeric: true, disablePadding: true, label: 'Severe Toxic' },
    { id: 'obscene', numeric: true, disablePadding: true, label: 'Obscene' },
    { id: 'threat', numeric: true, disablePadding: true, label: 'Threat' },
    { id: 'insult', numeric: true, disablePadding: true, label: 'Insult' },
    { id: 'identity_hate', numeric: true, disablePadding: true, label: 'Identity Hate' },
];



class EnhancedTableHead extends React.Component {
    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const { order, orderBy } = this.props;

        return (
            <TableHead>
                <TableRow>
                    {rows.map(row => {
                        const padding = row.id === 'id'
                        ? 'checkbox'
                        : row.disablePadding ? 'dense' : 'default';
                        return (
                            <TableCell
                                key={row.id}
                                numeric={row.numeric}
                                padding={padding}
                                sortDirection={orderBy === row.id ? order : false}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={100}
                                >
                                    <TableSortLabel
                                        active={orderBy === row.id}
                                        direction={order}
                                        onClick={this.createSortHandler(row.id)}
                                    >
                                        {row.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        );
    }
}

EnhancedTableHead.propTypes = {
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
};

export default EnhancedTableHead;