import os
import sys
import os
import re
import csv
import codecs
import numpy as np
import pandas as pd
import time
import warnings
from comment_cleaner import clean
# %matplotlib inline
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Dense, Input, LSTM, Embedding, Dropout, GlobalMaxPool1D
from keras.models import Model
from keras import initializers, regularizers, constraints, optimizers, layers
from util import *

warnings.filterwarnings('ignore')
os.environ['OMP_NUM_THREADS'] = '4'
start = time.time()

# PARAMETERS
max_features = 30000
embed_size = 300
maxlen = 100
batch_size = 32
epochs = 2


train = pd.read_csv('datasets/train.csv', encoding='latin-1')
test = pd.read_csv('datasets/test.csv', encoding='latin-1')
submission = pd.read_csv('sample_submission.csv')


list_classes = ['toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']
list_sentences_train = train['comment_text'].apply(lambda comment: clean(comment))
list_sentences_test = test['comment_text'].apply(lambda comment: clean(comment))
y = train[list_classes].values


tokenizer = Tokenizer(num_words=max_features)
tokenizer.fit_on_texts(list(list_sentences_train) + list(list_sentences_test))
list_tokenized_train = tokenizer.texts_to_sequences(list_sentences_train)
list_tokenized_test = tokenizer.texts_to_sequences(list_sentences_test)


X_t = pad_sequences(list_tokenized_train, maxlen=maxlen)
X_test = pad_sequences(list_tokenized_test, maxlen=maxlen)


embedding_matrix = get_embeddings(tokenizer, max_features, embed_size)
# OR:
# embedding_matrix = read_embeddings()


inp = Input(shape=(maxlen, ))
x = Embedding(max_features, embed_size, weights=[embedding_matrix], input_length=maxlen, trainable=False)(inp)
x = LSTM(60, return_sequences=True)(x)
x = GlobalMaxPool1D(name='GlobalMaxPool')(x)
x = Dropout(0.1)(x)
x = Dense(50, activation='relu')(x)
x = Dropout(0.1)(x)
x = Dense(6, activation='sigmoid')(x)


model = Model(inputs=inp, outputs=x)
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])


print('Training Model...')
start_fitting = time.time()
history = model.fit(X_t, y, batch_size=batch_size, epochs=epochs, validation_split=0.1)
fitting_model_time = time.time()
print('Training Model took: ', fitting_model_time - start_fitting)


# if we want to see graph of accuracy and loss
# get_accuracy_and_loss(history)


y_pred = model.predict(X_test, batch_size=1024)
submission[['toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']] = y_pred
submission.to_csv('submission_cleaned.csv', index=False)


model.save('models/model_fasttext.h5')
model.summary()


end = time.time()
print('TOTAL time spent', end-start)
