import numpy as np, pandas as pd
from keras.models import model_from_json
from keras.models import load_model
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

model = load_model('models/model_fasttext.h5')

train = pd.read_csv('datasets/train.csv', encoding='latin-1')
test = pd.read_csv('datasets/EE. All Reviews.csv', encoding='latin-1')
list_sentences_train = train['comment_text']
list_sentences_test = test['body']

max_features = 20000
tokenizer = Tokenizer(num_words=max_features)
tokenizer.fit_on_texts(list(list_sentences_train))
list_tokenized_test = tokenizer.texts_to_sequences(list_sentences_test)

maxlen = 100
X_test = pad_sequences(list_tokenized_test, maxlen=maxlen)
y_pred = model.predict(X_test, batch_size=1024)

pred_file = pd.DataFrame({
    'toxic':y_pred[:,0],
    'severe_toxic':y_pred[:,1],
    'obscene':y_pred[:,2],
    'threat':y_pred[:,3],
    'insult':y_pred[:,4],
    'identity_hate':y_pred[:,5]
})

submission = pd.concat(
    [test[['id']], 
    pred_file[['toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']]],
    axis=1
)

submission.to_json(path_or_buf='EE_preditions.csv', float_format='%.10f', index=False, unicode='utf-8')
