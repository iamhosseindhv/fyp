import os
import numpy as np
import matplotlib.pyplot as plt

EMBEDDING_FILE = './embeddings/crawl-300d-2M.vec'


def get_coefs(word, *arr): return word, np.asarray(arr, dtype='float32')


def get_embeddings(tokenizer, max_features, embed_size):
    print('Reading embeddings from file')
    embeddings_index = dict(get_coefs(*o.rstrip().rsplit(' '))
                            for o in open(EMBEDDING_FILE))
    word_index = tokenizer.word_index
    nb_words = min(max_features, len(word_index))
    embedding_matrix = np.zeros((nb_words, embed_size))
    for word, i in word_index.items():
        if i >= max_features:
            continue
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            embedding_matrix[i] = embedding_vector

    print('Saving embeddings to file')
    np.savetxt('embeddings/embeddings_array.csv',
               embedding_matrix, delimiter=",")

    return embedding_matrix


def read_embeddings():
    return np.genfromtxt('embeddings/embeddings_array.csv', delimiter=',')


def get_accuracy_and_loss(history):
    # Plot training & validation accuracy values
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validation'], loc='upper left')
    plt.show()

    # Plot training & validation loss values
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validation'], loc='upper left')
    plt.show()